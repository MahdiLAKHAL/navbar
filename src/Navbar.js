import React, { useState, useRef, useEffect } from 'react';
import { FaBars, FaTwitter, FaFacebook, FaLinkedin } from 'react-icons/fa';
import logo from './logo.svg';
import { links, social } from './data';
const Navbar = () => {
  const [showLinks, setShowLinks] = useState(false);
  const linksContainerRef = useRef(null);
  const linksRef = useRef(null);

  // this function allows us to toggle our menu with the approriate height depending on link's height
  useEffect(() => {
    // This function allows us te check geometrical value of our link
    const linksHeight = linksRef.current.getBoundingClientRect().height;
    if (showLinks) {
      linksContainerRef.current.style.height = `${linksHeight}px`;
    } else {
      linksContainerRef.current.style.height = '0px';
    }
  }, [showLinks]);
  return (
    <nav>
      <div className='nav-center'>
        <div className='nav-header'>
          <img src={logo} alt='logo' />
          <button
            className='nav-toggle'
            onClick={() => setShowLinks(!showLinks)}
          >
            <FaBars></FaBars>
          </button>
        </div>
        {/* we can use the conditionnal rendering to aply the class witch it is responsable for the transition of the toggle */}
        {/* <div
        className={`${
          showLinks ? 'links-container' : 'show-container links-container'
        }`}
        > */}

        <div className='links-container' ref={linksContainerRef}>
          <ul className='links' ref={linksRef}>
            {links.map((link) => {
              const { id, url, text } = link;
              return (
                <li key={id}>
                  <a href={url}>{text}</a>
                </li>
              );
            })}
          </ul>
        </div>

        <ul className='social-icons'>
          {social.map((icons) => {
            const { id, url, icon } = icons;
            return (
              <li key={id}>
                <a href={url}>{icon}</a>
              </li>
            );
          })}
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
